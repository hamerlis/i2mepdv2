// NOTE object below must be a valid JSON
window.I2MEPDMobile = $.extend(true, window.I2MEPDMobile, {
  "config": {
    "animationSet": "default",
    "navigation": [
      {
        "title": "Mijn afpraken",
        "onExecute": "#Appointments",
        "icon": "event"
      },
      {
        "title": "Medewerkers",
        "onExecute": "#Resources",
        "icon": "group"
      },
       {
           "title": "Home",
           "onExecute": "#Home",
           "icon": "group"
       },
      {
        "title": "Informatie",
        "onExecute": "#About",
        "icon": "info"
      },
    {
        "title": "Afmelden",
        "onExecute": function () {
            window.sessionStorage.token = null;
            I2MEPDMobile.loggedIn = false;
            I2MEPDMobile.app.navigate("login", { root: true });
        },
        "icon": "key"
    }
    ]
  }
});
