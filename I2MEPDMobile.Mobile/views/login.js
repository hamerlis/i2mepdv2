I2MEPDMobile.login = function (params) {
    I2MEPDMobile.loggedIn = false;
    var viewModel = {
        userID: ko.observable(""),
        password: ko.observable(""),       
        logIn: function () {
            $(document).click(function (evt) { evt.preventDefault; });
            var userID = this.userID(),
                password = this.password();

            userID = 'ADMIN'
            password = 'BmwMini01'
            //Authenticate a user here
            $("#small-indicator").dxLoadIndicator({
                height: 20,
                width: 20
            });

            var loginData = {
                grant_type: 'password',
                username: userID,
                password: password
            };

            function showError(jqXHR) {
                if (jqXHR.responseJSON && jqXHR.responseJSON.error_description) {
                    $(".erormsg").text(jqXHR.responseJSON.error_description);
                } else {
                    $(".erormsg").text("Onbekende fout:" + jqXHR.status + ': ' + jqXHR.statusText);
                }
            }
            

            $.ajax({
                type: 'POST',
                url: I2MEPDMobile.db._url + '/Token',
                data: loginData
            }).done(function (data) {
                // Cache the access token in session storage.  
                window.sessionStorage.token = data.access_token;
                I2MEPDMobile.loggedIn = true;
                I2MEPDMobile.personID = data.personID;
                I2MEPDMobile.clientID = data.clientID;
                I2MEPDMobile.app.navigate("Home", { root: true });
            }).fail(showError);

            $(document).off('click');
            $("#small-indicator").dxLoadIndicator({
                height: 0,
                width: 0
            });
        },
        close: function () {
            I2MEPDMobile.app.back();
        }
    };

    return viewModel;
};