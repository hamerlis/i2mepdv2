I2MEPDMobile.Appointments = function (params, viewInfo) {
    "use strict";

    var shouldReload = false,
        openCreateViewAsRoot = viewInfo.layoutController.name === "split",
        isReady = $.Deferred(),
        resources,
        resourcesObservable = ko.observable(),
        dataSourceObservable = ko.observable(),
        dataSource,
        filteredDate = ko.observable(new Date()),
        currentResourceID = ko.observable(I2MEPDMobile.User.ResourceID())

    function handleAppointmentsModification() {
        shouldReload = true;
    }

    function handleViewShowing() {
        if(!dataSourceObservable()) {
            dataSourceObservable(dataSource);
            dataSource.load().always(function() {
                isReady.resolve();
            });
        }
        else if(shouldReload) {
            refreshList();
        }
    }

    function handleViewDisposing() {
        I2MEPDMobile.db.Appointments.off("modified", handleAppointmentsModification);
    }

    function getApptFilter() {
        return [['day(Start)', "=", filteredDate().getDate()], ['month(Start)', "=", filteredDate().getMonth()], ['year(Start)', "=", filteredDate().getFullYear()], ['ResourceID', '=', currentResourceID()]];
    }

    function refreshList() {
        dataSource.filter(getApptFilter());
        shouldReload = false;
        dataSource.pageIndex(0);
        dataSource.load();
    }

    function onFilter(e) {
        refreshList();
    }

    function onFilterNavigate(dd) {
        var newDate = new Date();
        newDate = this.dateBoxValue();
        newDate.setDate(newDate.getDate() + dd);
        filteredDate(new Date(newDate));
    }

    dataSource = new DevExpress.data.DataSource({
        store: I2MEPDMobile.db.Appointments,
        map: function(item) {
            return new I2MEPDMobile.AppointmentsViewModel(item);
        },
        filter: getApptFilter(),
        group: ['Group'],
        sort: ['Start'],
        paginate: true
    });

    I2MEPDMobile.db.Appointments.on("modified", handleAppointmentsModification);

    resources = new DevExpress.data.DataSource({
        store: I2MEPDMobile.db.Resources, 
        filter: [['deleted', "=", "F"], ['ResourceClassID', "<>", "U"]],
        order: ['Sort'],
        paginate: false
    });

    return {
        isReady: isReady.promise(),
        dataSource: dataSourceObservable,
        refreshList: refreshList,
        resources: resources,
        currentResourceID: currentResourceID,
        viewShowing: handleViewShowing,
        viewDisposing: handleViewDisposing,
        openCreateViewAsRoot: openCreateViewAsRoot,
        onFilterDateValueChanged: onFilter,
        onFilterResourceValueChanged: onFilter,
        onFilterNavigate: onFilterNavigate,
        dateBoxValue: filteredDate,
        ApptClassDesc: dataSourceObservable.ApptClassDesc
        ,
        grouptemplate: function (groupData, groupIndex, groupElement) {
            groupElement.append('class="statuscolorbox" style="background-color:' + groupData.items[0].ApptClassColor() + ';margin-right:5px"</div>');
            groupElement.append(groupData.items[0].TimeSpan() + " " + groupData.items[0].ApptClassDesc())
        }
    };
};