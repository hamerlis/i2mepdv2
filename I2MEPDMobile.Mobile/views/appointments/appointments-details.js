I2MEPDMobile.AppointmentsDetails = function (params, viewInfo) {
    "use strict";
    var id = params.id,
        appointments = new I2MEPDMobile.AppointmentsViewModel(),
        isReady = $.Deferred(),
        persons, ApptID

    function handleDelete() {
        DevExpress.ui.dialog.confirm("Are you sure you want to delete this item?", "Delete item").then(function(result) {
            if(result)
                handleConfirmDelete();
        });
    }

    function handleConfirmDelete() {        
        I2MEPDMobile.db.Appointments.remove(id).done(function () {
            if(viewInfo.canBack) {
                I2MEPDMobile.app.navigate("appointments", { target: "back" });
            }
            else {
                I2MEPDMobile.app.navigate("Blank", { target: "current" });
            }
        });
    }
    var resources = new DevExpress.data.DataSource({
        store: I2MEPDMobile.db.ApptResource,
        filter: [['ApptID', "=", parseInt(id)], ['ResClassID', "<>", "U"]]
    })

    var facilities = new DevExpress.data.DataSource({
        store: I2MEPDMobile.db.ApptResource,
        filter: [['ApptID', "=", parseInt(id)], ['ResClassID', "=", "U"]]
    })

    var persons = new DevExpress.data.DataSource({
        store: I2MEPDMobile.db.ApptPerson,
        filter: ['ApptID', "=", parseInt(id)]
    })

    function handleViewShowing() {
        I2MEPDMobile.db.Appointments.byKey(id).done(function (data) {
            appointments.fromJS(data);
            isReady.resolve();
        });
    }
   

    return {
        id: id,
        appointments: appointments,
        persons: persons,
        resources: resources,
        facilities: facilities,
        handleDelete: handleDelete,        
        viewShowing: handleViewShowing,
        isReady: isReady.promise()
    };
};