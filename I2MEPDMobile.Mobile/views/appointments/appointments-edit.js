I2MEPDMobile.appointmentsEdit = function(params, viewInfo) {
    "use strict";

    var id = params.id,
        isNew = (id === undefined),
        isSplitLayout = viewInfo.layoutController.name === "split",
        appointments = new I2MEPDMobile.AppointmentsViewModel(),
        isReady = $.Deferred();

    function load() {
        return I2MEPDMobile.db.appointments.byKey(id).done(function(data) {
            appointments.fromJS(data);
        });
    }

    function update() {
        I2MEPDMobile.db.appointments.update(id, appointments.toJS()).done(function() {
            I2MEPDMobile.app.back();
        });
    }

    function insert() {
        I2MEPDMobile.db.appointments.insert(appointments.toJS()).done(function(values, newId) {
            I2MEPDMobile.app.navigate({ view: "appointmentsDetails", id: newId }, { target: "current" });
        });
    }

    function handleSave() {
        if(isNew)
            insert();
        else
            update();
    }

    function handleCancel() {
        if(!isNew) {
            I2MEPDMobile.app.back();
        }
        else {
            if(isSplitLayout) {
                I2MEPDMobile.app.navigate("Blank", { target: "current" });
            }
            else {
                I2MEPDMobile.app.back();
            }
        }
    }

    function handleViewShowing() {
        appointments.clear();
        if(!isNew)
            load().done(function() {
                isReady.resolve();
            });
        else {
            isReady.resolve();
        }
    }

    return {
        appointments: appointments,
        handleSave: handleSave,
        handleCancel: handleCancel,
        viewShowing: handleViewShowing,
        isReady: isReady.promise()
    };
};