I2MEPDMobile.PatientenEdit = function(params, viewInfo) {
    "use strict";

    var id = params.id,
        isNew = (id === undefined),
        isSplitLayout = viewInfo.layoutController.name === "split",
        patienten = new I2MEPDMobile.PatientenViewModel(),
        isReady = $.Deferred();

    function load() {
        return I2MEPDMobile.db.Patienten.byKey(id).done(function(data) {
            patienten.fromJS(data);
        });
    }

    function update() {
        I2MEPDMobile.db.Patienten.update(id, patienten.toJS()).done(function() {
            I2MEPDMobile.app.back();
        });
    }

    function insert() {
        I2MEPDMobile.db.Patienten.insert(patienten.toJS()).done(function(values, newId) {
            I2MEPDMobile.app.navigate({ view: "PatientenDetails", id: newId }, { target: "current" });
        });
    }

    function handleSave() {
        if(isNew)
            insert();
        else
            update();
    }

    function handleCancel() {
        if(!isNew) {
            I2MEPDMobile.app.back();
        }
        else {
            if(isSplitLayout) {
                I2MEPDMobile.app.navigate("Blank", { target: "current" });
            }
            else {
                I2MEPDMobile.app.back();
            }
        }
    }

    function handleViewShowing() {
        patienten.clear();
        if(!isNew)
            load().done(function() {
                isReady.resolve();
            });
        else {
            isReady.resolve();
        }
    }

    return {
        patienten: patienten,
        handleSave: handleSave,
        handleCancel: handleCancel,
        viewShowing: handleViewShowing,
        isReady: isReady.promise()
    };
};