I2MEPDMobile.PatientenDetails = function(params, viewInfo) {
    "use strict";

    var id = params.id,
        patienten = new I2MEPDMobile.PatientenViewModel(),
        isReady = $.Deferred();

    function handleDelete() {
        DevExpress.ui.dialog.confirm("Are you sure you want to delete this item?", "Delete item").then(function(result) {
            if(result)
                handleConfirmDelete();
        });
    }

    function handleConfirmDelete() {        
        I2MEPDMobile.db.Patienten.remove(id).done(function() {
            if(viewInfo.canBack) {
                I2MEPDMobile.app.navigate("Patienten", { target: "back" });
            }
            else {
                I2MEPDMobile.app.navigate("Blank", { target: "current" });
            }
        });
    }

    function handleViewShowing() {
        I2MEPDMobile.db.Patienten.byKey(id).done(function(data) {
            patienten.fromJS(data);
            isReady.resolve();
        });
    }

    return {
        id: id,
        patienten: patienten,
        handleDelete: handleDelete,        
        viewShowing: handleViewShowing,
        isReady: isReady.promise()
    };
};