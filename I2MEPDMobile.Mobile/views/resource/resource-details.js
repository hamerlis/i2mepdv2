I2MEPDMobile.ResourceDetails = function(params, viewInfo) {
    "use strict";

    var id = params.id,
        resource = new I2MEPDMobile.ResourceViewModel(),
        isReady = $.Deferred();

    function handleDelete() {
        DevExpress.ui.dialog.confirm("Are you sure you want to delete this item?", "Delete item").then(function(result) {
            if(result)
                handleConfirmDelete();
        });
    }

    function handleConfirmDelete() {        
        I2MEPDMobile.db.Resources.remove(id).done(function() {
            if(viewInfo.canBack) {
                I2MEPDMobile.app.navigate("Resources", { target: "back" });
            }
            else {
                I2MEPDMobile.app.navigate("Blank", { target: "current" });
            }
        });
    }

    function handleViewShowing() {
        I2MEPDMobile.db.Resources.byKey(id).done(function(data) {
            resource.fromJS(data);
            isReady.resolve();
        });
    }

    return {
        id: id,
        resource: resource,
        handleDelete: handleDelete,        
        viewShowing: handleViewShowing,
        isReady: isReady.promise()
    };
};