I2MEPDMobile.ResourceEdit = function(params, viewInfo) {
    "use strict";

    var id = params.id,
        isNew = (id === undefined),
        isSplitLayout = viewInfo.layoutController.name === "split",
        resource = new I2MEPDMobile.ResourceViewModel(),
        isReady = $.Deferred();

    function load() {
        return I2MEPDMobile.db.Resources.byKey(id).done(function(data) {
            resource.fromJS(data);
        });
    }

    function update() {
        I2MEPDMobile.db.Resources.update(id, resource.toJS()).done(function() {
            I2MEPDMobile.app.back();
        });
    }

    function insert() {
        I2MEPDMobile.db.Resources.insert(resource.toJS()).done(function(values, newId) {
            I2MEPDMobile.app.navigate({ view: "ResourceDetails", id: newId }, { target: "current" });
        });
    }

    function handleSave() {
        if(isNew)
            insert();
        else
            update();
    }

    function handleCancel() {
        if(!isNew) {
            I2MEPDMobile.app.back();
        }
        else {
            if(isSplitLayout) {
                I2MEPDMobile.app.navigate("Blank", { target: "current" });
            }
            else {
                I2MEPDMobile.app.back();
            }
        }
    }

    function handleViewShowing() {
        resource.clear();
        if(!isNew)
            load().done(function() {
                isReady.resolve();
            });
        else {
            isReady.resolve();
        }
    }

    return {
        resource: resource,
        handleSave: handleSave,
        handleCancel: handleCancel,
        viewShowing: handleViewShowing,
        isReady: isReady.promise()
    };
};