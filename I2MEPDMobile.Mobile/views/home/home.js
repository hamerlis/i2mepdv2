﻿I2MEPDMobile.Home = function (params, viewInfo) {
    var user = new I2MEPDMobile.UserViewModel(),
        isReady = $.Deferred()

    function handleappointmentsModification() {
        shouldReload = true;
    }

    function WelcomeMsg() {
        var uur = new Date().getHours();
        if (uur >= 18)
            return "Goedenavond " + user.VolledigeNaam()
        else if (uur >= 12)
            return "Goedemiddag " + user.VolledigeNaam()
        else if (uur >= 6)
            return "Goedemorgen " + user.VolledigeNaam()
        else
            return "Goedenacht " + user.VolledigeNaam()
    }

    function handleViewShowing() {
        I2MEPDMobile.db.Users.byKey('ADMIN').done(function (data) {
            user.fromJS(data);
            I2MEPDMobile.User = new I2MEPDMobile.UserViewModel(),
            I2MEPDMobile.User.fromJS(data);
            isReady.resolve();
        });
    }

    return {
        viewShowing: handleViewShowing,
        isReady: isReady.promise(),
        user: user,
        WelcomeMsg:WelcomeMsg
    };
};