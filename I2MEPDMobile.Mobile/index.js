$(function () {
      
    // Uncomment the line below to disable platform-specific look and feel and to use the Generic theme for all devices
    // DevExpress.devices.current({ platform: "generic" });

    if(DevExpress.devices.real().platform === "win") {
        $("body").css("background-color", "#000");
    }

    $(document).on("deviceready", function () {
        navigator.splashscreen.hide();
        if (window.devextremeaddon) {
            window.devextremeaddon.setup();
        }
        $(document).on("backbutton", function () {
            DevExpress.processHardwareBackButton();
        });
    });

    function onNavigatingBack(e) {
        if(e.isHardwareButton && !I2MEPDMobile.app.canBack()) {
            e.cancel = true;
            exitApp();
        }
    }

    function exitApp() {
        switch (DevExpress.devices.real().platform) {
            case "android":
                navigator.app.exitApp();
                break;
            case "win":
                MSApp.terminateApp('');
                break;
        }
    }

    var navigation = I2MEPDMobile.config.navigation;

    I2MEPDMobile.slideOutController = new DevExpress.framework.html.SlideOutController
    I2MEPDMobile.emptyController = new DevExpress.framework.html.EmptyLayoutController;

    I2MEPDMobile.app = new DevExpress.framework.html.HtmlApplication({
        namespace: I2MEPDMobile,
        animationSet: DevExpress.framework.html.animationSets['default'],
        layoutSet: [
               { controller: I2MEPDMobile.emptyController },
               { customResolveRequired: true, controller: I2MEPDMobile.slideOutController }
           ],
        animationSet: DevExpress.framework.html.animationSets[I2MEPDMobile.config.animationSet],
        navigation: navigation,
        commandMapping: I2MEPDMobile.config.commandMapping,
        navigateToRootViewMode: "keepHistory",
        useViewTitleAsBackText: true
    });

    $(window).unload(function() {
        I2MEPDMobile.app.saveState();
    });
    
    I2MEPDMobile.app.router.register(":view/:id", { view: "Appointments", id: undefined });

    I2MEPDMobile.app.router.register("login/:backUri");

    I2MEPDMobile.app.on("initialized", function () {
        I2MEPDMobile.loggedIn = false;
    });

    I2MEPDMobile.app.on("navigating", function (e) {
        var params = I2MEPDMobile.app.router.parse(e.uri),
            viewInfo = I2MEPDMobile.app.getViewTemplateInfo(params.view);
        if (viewInfo.secure && !I2MEPDMobile.loggedIn) {
            e.cancel = true;
            I2MEPDMobile.app.navigate({ view: "login", backUri: e.uri });
        }
    });

        I2MEPDMobile.app.on("resolveLayoutController", function (args) {
            var viewName = args.viewInfo.viewName;
            if (viewName === "login") {
                args.layoutController = I2MEPDMobile.emptyController
            } else {
                args.layoutController = I2MEPDMobile.slideOutController
            }
        });

    I2MEPDMobile.app.on("navigatingBack", onNavigatingBack);
    I2MEPDMobile.app.navigate();
});