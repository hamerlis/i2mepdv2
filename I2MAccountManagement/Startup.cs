﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(I2M_AccountManagement.Startup))]
namespace I2M_AccountManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
