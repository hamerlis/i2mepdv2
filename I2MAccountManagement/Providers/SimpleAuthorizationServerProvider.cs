using I2MEFDatamodel;
using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace I2M_AccountManagement
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public enum LogonResults
        {
            DENIED,
            OK,
            NOROLES,
            FIRST,
            LOCKED,
            NOTACTIVATED,
            NOTRANSACTIONS,
            NOTACTIVE,
            FAILURE,
            USERNOTKNOWN,
            NOVALIDTOKEN,
            VALIDATETOKEN,
            EXPIRED
        }

        public av_MobUsers cUser;
        private string CalculateMD5Hash(string input)

        {

            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {

                sb.Append(hash[i].ToString("X2"));

            }

            return sb.ToString();

        }

        public bool acceptedLogonStatus(LogonResults LogonResult)
        {
            return LogonResult == LogonResults.OK | LogonResult == LogonResults.FIRST | LogonResult == LogonResults.NOVALIDTOKEN | LogonResult == LogonResults.VALIDATETOKEN | LogonResult == LogonResults.EXPIRED;
        }

        public string LogonStatusText(LogonResults LogonResult)
        {
            switch (LogonResult)
            {
                case LogonResults.NOTRANSACTIONS:
                    return "Geen transacties voor uw rol opgezet.";
                case LogonResults.NOROLES:
                    return "Geen rol gekoppeld aan uw profiel.";
                case LogonResults.LOCKED:
                    return "Toegang geweigerd (gebruiker geblokkeerd).";
                case LogonResults.NOTACTIVE:
                    return "Toegang geweigerd (gebruiker niet actief).";
                case LogonResults.FIRST:
                    return "Eerste aanmelding";
                case LogonResults.USERNOTKNOWN:
                    return "Gebruiker niet bekend, of verkeerd wachtwoord.";
                case LogonResults.FAILURE:
                    return "Fout bij aanmelden";
                case LogonResults.EXPIRED:
                    return "Wachtwoord verlopen";
                case LogonResults.OK:
                    //All well
                    return "OK";
                default:
                    return "Ongeldige gebruikersnaam of wachtwoord.";
            }
        }
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            if (context.Password == null || context.Password == "") { 
                context.SetError("invalid_grant", "Geef gebruikersnaam en wachtwoord.");
                    return;
                }

            string passwdMD5 = CalculateMD5Hash(context.Password);

            System.Data.Entity.Core.Objects.ObjectParameter sResult = new System.Data.Entity.Core.Objects.ObjectParameter("Result",0);
            System.Data.Entity.Core.Objects.ObjectParameter sInternalUserID = new System.Data.Entity.Core.Objects.ObjectParameter("InternalUserID", "");
            System.Data.Entity.Core.Objects.ObjectParameter sRoleDesc = new System.Data.Entity.Core.Objects.ObjectParameter("RoleDesc", "");
            System.Data.Entity.Core.Objects.ObjectParameter iClinicOrgCode = new System.Data.Entity.Core.Objects.ObjectParameter("ClinicOrgCode", 0);
            System.Data.Entity.Core.Objects.ObjectParameter iRoleID = new System.Data.Entity.Core.Objects.ObjectParameter("RoleID",0);
            string clientAddress = context.Request.RemoteIpAddress;

            var db = new intomediEntities();
            db.sp_get_Autentication(context.UserName.ToUpper(), passwdMD5, "Forms", clientAddress, sResult, sInternalUserID, iClinicOrgCode, iRoleID, sRoleDesc);

            LogonResults iLoginResult = (LogonResults)System.Enum.Parse(typeof(LogonResults), sResult.Value.ToString());

            if (!acceptedLogonStatus(iLoginResult))
            {
                context.SetError("invalid_grant", LogonStatusText(iLoginResult));
                return;
            }

            cUser = db.av_MobUsers.SingleOrDefault(av_MobUsers => av_MobUsers.UserID.ToUpper() == context.UserName.ToUpper());

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("sub", context.UserName));
            identity.AddClaim(new Claim("roleID", iRoleID.Value.ToString()));
            identity.AddClaim(new Claim("clientID", cUser.ClientID.ToString()));

            context.Validated(identity);

        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            if (cUser != null)
            {
                context.AdditionalResponseParameters.Add("clientID", cUser.ClientID);
                context.AdditionalResponseParameters.Add("personID", cUser.PersonID);
            }

            return Task.FromResult<object>(null);
        }
    }
}