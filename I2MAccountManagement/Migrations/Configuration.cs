namespace I2M_EPD2.Migrations
{
    using I2M_AccountManagement.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            context.Users.AddOrUpdate(
              p => p.UserName,
              new ApplicationUser { UserName = "Admin" }
            );

        }
    }
}
