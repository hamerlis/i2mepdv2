﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using I2MEFDatamodel;
using System.Web.Http.Cors;
using System.Net.Http.Formatting;
using Newtonsoft.Json.Serialization;

namespace I2MEPD_ODataService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
            config.MapHttpAttributeRoutes();
            config.Filters.Add(new AuthorizeAttribute());

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<av_MobResources>("Resources");
            builder.EntitySet<av_Patienten>("Patienten");
            builder.EntitySet<av_MobAppointments>("Appointments");
            builder.EntitySet<av_MobApptPerson>("ApptPerson");
            builder.EntitySet<av_MobApptResource>("ApptResource");
            builder.EntitySet<av_MobUsers>("Users");
            builder.EntitySet<av_MobUsers>("Users").EntityType.Ignore(ui => ui.Wachtwoord);
            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
            
        }
    }
}
