﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using I2MEFDatamodel;

namespace I2MEPD_ODataService.Controllers.api
{

    public class PatientenController : ODataController
    {
        private intomediEntities db = new intomediEntities();

        // GET: odata/Patienten
        [EnableQuery]
        [Authorize]
        public IQueryable<av_Patienten> GetPatienten()
        {
            return db.av_Patienten;
        }

        // GET: odata/Patienten(5)
        [EnableQuery]
        [Authorize]
        public SingleResult<av_Patienten> Getav_Patienten([FromODataUri] long key)
        {
            return SingleResult.Create(db.av_Patienten.Where(av_Patienten => av_Patienten.PersonID == key));
        }

    }
}
