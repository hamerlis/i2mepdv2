﻿using I2MEFDatamodel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;

namespace I2MEPD_ODataService.Controllers.api
{
    public class ApptResourceController : ODataController
    {
        private intomediEntities db = new intomediEntities();

        // GET: odata/resources
        [Authorize]
        [EnableQuery]
        public IQueryable<av_MobApptResource> GetApptResource()
        {
            //return db.av_MobAppointments.Where(av_MobAppointments => av_MobAppointments.ApptID < 20);
            return db.av_MobApptResource;
        }

        // GET: odata/ApptPerson(5) -> Get all resources of appointment
        [Authorize]
        [EnableQuery]
        public IQueryable<av_MobApptResource> Getav_MobApptResource([FromODataUri] int key)
        {
            return db.av_MobApptResource.Where(av_MobApptResource => av_MobApptResource.ApptID == key);
        }
    }

}
