﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using I2MEFDatamodel;

namespace I2MEPD_ODataService.Controllers.api
{
    public class ApptPersonController : ODataController
    {
        private intomediEntities db = new intomediEntities();

        // GET: odata/Appointments
        [Authorize]
        [EnableQuery]
        public IQueryable<av_MobApptPerson> GetApptPerson()
        {
            //return db.av_MobAppointments.Where(av_MobAppointments => av_MobAppointments.ApptID < 20);
            return db.av_MobApptPerson;
        }

        // GET: odata/ApptPerson(5) -> Get all persons of appointment
        [Authorize]
        [EnableQuery]
        public IQueryable<av_MobApptPerson> Getav_MobApptPerson([FromODataUri] int key)
        {
            return db.av_MobApptPerson.Where(av_MobApptPerson => av_MobApptPerson.ApptID == key);
        }
    }

}
