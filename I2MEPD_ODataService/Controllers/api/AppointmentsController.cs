﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using I2MEFDatamodel;
using System.Web.Http.Filters;

namespace I2MEPD_ODataService.Controllers.api
{
    //[HttpBasicAuthorize]
    //[RequireHttps]
    public class AppointmentsController : ODataController
    {
        private I2MEFDatamodel.intomediEntities db = new I2MEFDatamodel.intomediEntities();

        // GET: odata/Appointments
        [Authorize]
        [EnableQuery]
        public IQueryable<av_MobAppointments> GetAppointments()
        {
            //return db.av_MobAppointments.Where(av_MobAppointments => av_MobAppointments.ApptID < 20);
            return db.av_MobAppointments;

        }

        // GET: odata/Appointments(5)
        [EnableQuery]
        public SingleResult<av_MobAppointments> Getav_MobAppointments([FromODataUri] long key)
        {
            return SingleResult.Create(db.av_MobAppointments.Where(av_MobAppointments => av_MobAppointments.ApptID == key));
        }

        // GET /odata(1)/Persons (of an appointment)
        [EnableQuery]
        public IQueryable<av_MobApptPerson> GetPersons([FromODataUri] int key)
        {
            return db.av_MobApptPerson.Where(av_MobApptPerson => av_MobApptPerson.ApptID == key);
        }

        // GET /odata(1)/Resources  (of an appointment)
        [EnableQuery]
        public IQueryable<av_MobApptResource> GetResources([FromODataUri] int key)
        {
            return db.av_MobApptResource.Where(av_MobApptResource => av_MobApptResource.ApptID == key);
        }


        // PUT: odata/Appointments(5)
        public IHttpActionResult Put([FromODataUri] long key, Delta<av_MobAppointments> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            av_MobAppointments av_MobAppointments = db.av_MobAppointments.Find(key);
            if (av_MobAppointments == null)
            {
                return NotFound();
            }

            patch.Put(av_MobAppointments);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AppointmentExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(av_MobAppointments);
        }

        // POST: odata/Appointment
        public IHttpActionResult Post(av_MobAppointments Appointment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.av_MobAppointments.Add(Appointment);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (AppointmentExists(Appointment.ApptID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(Appointment);
        }

        // PATCH: odata/Appointments(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] long key, Delta<av_MobAppointments> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            av_MobAppointments Appointment = db.av_MobAppointments.Find(key);
            if (Appointment == null)
            {
                return NotFound();
            }

            patch.Patch(Appointment);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AppointmentExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(Appointment);
        }

        // DELETE: odata/Appointments(5)
        public IHttpActionResult Delete([FromODataUri] long key)
        {
            av_MobAppointments Appointment = db.av_MobAppointments.Find(key);
            if (Appointment == null)
            {
                return NotFound();
            }

            db.av_MobAppointments.Remove(Appointment);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AppointmentExists(long key)
        {
            return db.av_MobAppointments.Count(e => e.ApptID == key) > 0;
        }
    }
}
