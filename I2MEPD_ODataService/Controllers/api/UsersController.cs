﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using I2MEFDatamodel;

namespace I2MEPD_ODataService.Controllers.api
{
    public class UsersController : ODataController
    {
        private intomediEntities db = new intomediEntities();

        // GET: odata/Users
        [EnableQuery]
        public IQueryable<av_MobUsers> GetUsers()
        {
            return db.av_MobUsers;
        }

        // GET: odata/Users(5)
        [EnableQuery]
        public SingleResult<av_MobUsers> Getav_MobUsers([FromODataUri] string key)
        {
            return SingleResult.Create(db.av_MobUsers.Where(av_MobUsers => av_MobUsers.UserID == key));
        }

        
    }
}
