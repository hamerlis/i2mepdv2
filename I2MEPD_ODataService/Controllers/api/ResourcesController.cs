﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using I2MEFDatamodel;

namespace I2MEPD_ODataService.Controllers.api
{
    public class ResourcesController : ODataController
    {
        private intomediEntities db = new intomediEntities();

        // GET: odata/Resources
        [EnableQuery]
        public IQueryable<av_MobResources> GetResources()
        {
            return db.av_MobResources;
        }

        // GET: odata/Resources(5)
        [EnableQuery]
        public SingleResult<av_MobResources> Getav_MobResources([FromODataUri] int key)
        {
            return SingleResult.Create(db.av_MobResources.Where(av_MobResources => av_MobResources.ResourceID == key));
        }

        
    }
}
