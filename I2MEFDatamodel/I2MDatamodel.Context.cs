﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace I2MEFDatamodel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class intomediEntities : DbContext
    {
        public intomediEntities()
            : base("name=intomediEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<av_MobAppointments> av_MobAppointments { get; set; }
        public virtual DbSet<av_MobApptPerson> av_MobApptPerson { get; set; }
        public virtual DbSet<av_MobApptResource> av_MobApptResource { get; set; }
        public virtual DbSet<av_MobResources> av_MobResources { get; set; }
        public virtual DbSet<av_MobUsers> av_MobUsers { get; set; }
        public virtual DbSet<av_Patienten> av_Patienten { get; set; }
    
        public virtual int sp_get_Autentication(string userID, string password, string type, string iPnbr, ObjectParameter result, ObjectParameter internalUserID, ObjectParameter clinicOrgCode, ObjectParameter roleID, ObjectParameter roleDesc)
        {
            var userIDParameter = userID != null ?
                new ObjectParameter("UserID", userID) :
                new ObjectParameter("UserID", typeof(string));
    
            var passwordParameter = password != null ?
                new ObjectParameter("Password", password) :
                new ObjectParameter("Password", typeof(string));
    
            var typeParameter = type != null ?
                new ObjectParameter("Type", type) :
                new ObjectParameter("Type", typeof(string));
    
            var iPnbrParameter = iPnbr != null ?
                new ObjectParameter("IPnbr", iPnbr) :
                new ObjectParameter("IPnbr", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_get_Autentication", userIDParameter, passwordParameter, typeParameter, iPnbrParameter, result, internalUserID, clinicOrgCode, roleID, roleDesc);
        }
    }
}
