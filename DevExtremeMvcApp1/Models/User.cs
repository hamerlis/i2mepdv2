﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Security;

namespace DevExtremeMvcApp1.Models
{
    public class User:SuperClass
    {
        private DevExtremeMvcApp1.User CDBUser;
        private DevExtremeMvcApp1.Persoon CDBPersoon;
        private intomediEntities db = new intomediEntities();

        public DevExtremeMvcApp1.User DBUser
        {
            get
            {
                 
                if (CDBUser == null)
                {
                    CDBUser = db.User.Where(b => b.UserID == this.UserID).FirstOrDefault();
                }
                return CDBUser;
            }
            set
            {
                CDBUser = value;
            }
        }

        public DevExtremeMvcApp1.Persoon DBPersoon
        {
            get
            {
                if (CDBUser == null)
                {
                    CDBPersoon = db.Persoon.Where(b => b.PersonID == this.PersonID).FirstOrDefault();
                }
                return CDBPersoon;
            }
            set
            {
                CDBPersoon = value;
            }
        }

        public string TelephoneMobile { get { return "123"; } }
        public string Email { get { return "123"; } }
        public long PersonID { get {
                if (CDBUser == null) { return 0; }
                    long? i = DBUser.PersonID;
                bool isINull = i == null;
                return (long)DBUser.PersonID;} }

        [Required]
        [Display(Name = "User ID")]
        public string UserID { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember on this computer")]
        public bool RememberMe { get; set; }

        public int ClinicOrgCode;

        public enum LogonResults
        {
            DENIED,
            OK,
            NOROLES,
            FIRST,
            LOCKED,
            NOTACTIVATED,
            NOTRANSACTIONS,
            NOTACTIVE,
            FAILURE,
            USERNOTKNOWN,
            NOVALIDTOKEN,
            VALIDATETOKEN,
            EXPIRED
        }

        public bool acceptedLogonStatus(LogonResults LogonResult)
        {
            return LogonResult == LogonResults.OK | LogonResult == LogonResults.FIRST | LogonResult == LogonResults.NOVALIDTOKEN | LogonResult == LogonResults.VALIDATETOKEN | LogonResult == LogonResults.EXPIRED;
        }

        public string LogonStatusText(LogonResults LogonResult)
        {
            switch (LogonResult)
            {
                case User.LogonResults.NOTRANSACTIONS:
                    return "Geen transacties voor uw rol opgezet.";
                case User.LogonResults.NOROLES:
                    return "Geen rol gekoppeld aan uw profiel.";
                case User.LogonResults.LOCKED:
                    return "Toegang geweigerd (gebruiker geblokkeerd).";
                case User.LogonResults.NOTACTIVE:
                    return "Toegang geweigerd (gebruiker niet actief).";
                case User.LogonResults.FIRST:
                    return "Eerste aanmelding";
                case LogonResults.USERNOTKNOWN:
                    return "Gebruiker niet bekend, of verkeerd wachtwoord.";
                case User.LogonResults.FAILURE:
                    return "Fout bij aanmelden";
                case User.LogonResults.EXPIRED:
                    return "Wachtwoord verlopen";
                case User.LogonResults.OK:
                    //All well
                    return "OK";
                default:
                    return "Ongeldige gebruikersnaam of wachtwoord.";
            }
        }

        public LogonResults Logon(string sUserID, string sPassword, int iClinicOrgCode = 0, int iRoleID = 0) 
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_get_autentication", dbConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@userID", SqlDbType.NVarChar, 250).Value = "ADMIN";
                cmd.Parameters.Add("@password", SqlDbType.NVarChar, 40).Value = SHA5.Encode("BmwMini01");
                cmd.Parameters.Add("@Type", SqlDbType.NVarChar, 20).Value = "Forms";
                cmd.Parameters.Add("@IPnbr", SqlDbType.NVarChar, 50).Value = "1";

                System.Data.SqlClient.SqlParameter iResult = new System.Data.SqlClient.SqlParameter("@Result", SqlDbType.Int);
                iResult.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(iResult);

                System.Data.SqlClient.SqlParameter pInternalUserID = new System.Data.SqlClient.SqlParameter("@InternalUserID", SqlDbType.NVarChar, 8);
                pInternalUserID.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pInternalUserID);

                System.Data.SqlClient.SqlParameter pClinicOrgCode = new System.Data.SqlClient.SqlParameter("@ClinicOrgCode", SqlDbType.BigInt);
                pClinicOrgCode.Direction = ParameterDirection.InputOutput;
                pClinicOrgCode.Value = iClinicOrgCode > 0 ? iClinicOrgCode : ClinicOrgCode;
                cmd.Parameters.Add(pClinicOrgCode);

                System.Data.SqlClient.SqlParameter pRoleID = new System.Data.SqlClient.SqlParameter("@RoleID", SqlDbType.Int);
                pRoleID.Direction = ParameterDirection.InputOutput;
                pRoleID.Value = iRoleID;
                cmd.Parameters.Add(pRoleID);

                System.Data.SqlClient.SqlParameter pRoleDesc = new System.Data.SqlClient.SqlParameter("@RoleDesc", SqlDbType.NVarChar, 50);
                pRoleDesc.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pRoleDesc);

                if (dbConnection.State == ConnectionState.Closed)
                    dbConnection.Open();
                cmd.ExecuteNonQuery();

                

               // if (acceptedLogonStatus((LogonResults)Enum.ToObject(typeof(LogonResults), iResult))
                //{
                //    this.UserID = pInternalUserID.Value;
                //    this.ClinicOrgCode = pClinicOrgCode.Value;
                //    this.Role = pRoleID.Value;
                //    this.RoleDesc = pRoleDesc.Value;
                //}

                return (LogonResults)Enum.ToObject(typeof(LogonResults), iResult.Value);

            }
            catch (Exception e)
            {
                return LogonResults.FAILURE;
            }

        }

        //=======================================================
        //Service provided by Telerik (www.telerik.com)
        //Conversion powered by NRefactory.
        //Twitter: @telerik
        //Facebook: facebook.com/telerik
        //=======================================================



        public class SHA5
        {
            public static string Encode(string value)
            {
                MD5 md5 = new MD5CryptoServiceProvider();
                Byte[] originalBytes = System.Text.ASCIIEncoding.Default.GetBytes(value);
                Byte[] encodedBytes = md5.ComputeHash(originalBytes);

                return BitConverter.ToString(encodedBytes).Replace("-", "").ToLower();
            }
        }
        
        //public bool IsValid(string _username, string _password)
        //{
        //    using (var cn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename" +
        //      @"='C:\Tutorials\1 - Creating a custom user login form\Creating " +
        //      @"a custom user login form\App_Data\Database1.mdf';Integrated Security=True"))
        //    {
        //        string _sql = @"SELECT [Username] FROM [dbo].[System_Users] " +
        //               @"WHERE [Username] = @u AND [Password] = @p";
        //        var cmd = new SqlCommand(_sql, cn);
        //        cmd.Parameters
        //            .Add(new SqlParameter("@u", SqlDbType.NVarChar))
        //            .Value = _username;
        //        cmd.Parameters
        //            .Add(new SqlParameter("@p", SqlDbType.NVarChar))
        //            .Value = Helpers.SHA1.Encode(_password);
        //        cn.Open();
        //        var reader = cmd.ExecuteReader();
        //        if (reader.HasRows)
        //        {
        //            reader.Dispose();
        //            cmd.Dispose();
        //            return true;
        //        }
        //        else
        //        {
        //            reader.Dispose();
        //            cmd.Dispose();
        //            return false;
        //        }
        //    }
        //}
    }

}