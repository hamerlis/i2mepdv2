﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DevExtremeMvcApp1.Models
{
    public class SuperClass
    {
        SqlConnection cdbConnection;

        public SqlConnection dbConnection
        {
            get { if (cdbConnection == null)
                {
                    cdbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["IntoCareConnection"].ToString());
                }
                    return cdbConnection; }
        }


    }
}