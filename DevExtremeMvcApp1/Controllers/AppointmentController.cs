﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DevExtremeMvcApp1;

namespace DevExtremeMvcApp1.Controllers
{
    public class AppointmentController : Controller
    {
        private intomediEntities db = new intomediEntities();

        // GET: av_AppointmentsWidget
        public ActionResult Index()
        {
            return View(db.av_AppointmentsWidget.ToList());
        }

        // GET: av_AppointmentsWidget/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            av_AppointmentsWidget av_AppointmentsWidget = db.av_AppointmentsWidget.Find(id);
            if (av_AppointmentsWidget == null)
            {
                return HttpNotFound();
            }
            return View(av_AppointmentsWidget);
        }

        // GET: av_AppointmentsWidget/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: av_AppointmentsWidget/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TimeSpan,ApptClassCode,Group,ApptID,ClientID,Start,End,StatusID,Description,ApptClassID,ChangedOn,CreatedOn,OrgCode,ChangedBy,ResourceDesc,VolledigeNaam,ID,Datum,Label,ResourceID,ApptStatus,ApptStatusColor,ApptClassColor,PatientPersonID,LocationName,Cancelled,PaStatus,PaStColor,PaCancelled,PaStatusID,CreatedBy,ResourceClassID,ResourceAbbreviation,ApptClassDesc,ResourceInfo,PatientMissingData,PatientRemarks")] av_AppointmentsWidget av_AppointmentsWidget)
        {
            if (ModelState.IsValid)
            {
                db.av_AppointmentsWidget.Add(av_AppointmentsWidget);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(av_AppointmentsWidget);
        }

        // GET: av_AppointmentsWidget/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            av_AppointmentsWidget av_AppointmentsWidget = db.av_AppointmentsWidget.Find(id);
            if (av_AppointmentsWidget == null)
            {
                return HttpNotFound();
            }
            return View(av_AppointmentsWidget);
        }

        // POST: av_AppointmentsWidget/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TimeSpan,ApptClassCode,Group,ApptID,ClientID,Start,End,StatusID,Description,ApptClassID,ChangedOn,CreatedOn,OrgCode,ChangedBy,ResourceDesc,VolledigeNaam,ID,Datum,Label,ResourceID,ApptStatus,ApptStatusColor,ApptClassColor,PatientPersonID,LocationName,Cancelled,PaStatus,PaStColor,PaCancelled,PaStatusID,CreatedBy,ResourceClassID,ResourceAbbreviation,ApptClassDesc,ResourceInfo,PatientMissingData,PatientRemarks")] av_AppointmentsWidget av_AppointmentsWidget)
        {
            if (ModelState.IsValid)
            {
                db.Entry(av_AppointmentsWidget).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(av_AppointmentsWidget);
        }

        // GET: av_AppointmentsWidget/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            av_AppointmentsWidget av_AppointmentsWidget = db.av_AppointmentsWidget.Find(id);
            if (av_AppointmentsWidget == null)
            {
                return HttpNotFound();
            }
            return View(av_AppointmentsWidget);
        }

        // POST: av_AppointmentsWidget/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            av_AppointmentsWidget av_AppointmentsWidget = db.av_AppointmentsWidget.Find(id);
            db.av_AppointmentsWidget.Remove(av_AppointmentsWidget);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: av_AppointmentsWidget
        public ActionResult Widget()
        {
            return View("Widget");
        }
    }
}
