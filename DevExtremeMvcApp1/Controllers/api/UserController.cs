﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace DevExtremeMvcApp1.Controllers.api
{
        public class UserController : Controller
        {
            //
            // GET: /User/
            public ActionResult Index()
            {
                return View();
            }

            [HttpGet]
            public ActionResult Login()
            {
                return View();
            }

            [HttpPost]
            public ActionResult Login(Models.User user)
            {
            Exception ex = new Exception();
                if (ModelState.IsValid)
                {
                Models.User.LogonResults result = user.Logon(user.UserID, user.Password);

                if(user.acceptedLogonStatus(result))
                    {
                        FormsAuthentication.SetAuthCookie(user.UserID, user.RememberMe);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("", user.LogonStatusText(result));
                    }
                }
                return View(user);
            }
            public ActionResult Logout()
            {
                FormsAuthentication.SignOut();
                return RedirectToAction("Index", "Home");
            }
        }

    }