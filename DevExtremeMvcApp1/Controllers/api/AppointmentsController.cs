﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using DevExtremeMvcApp1;

namespace DevExtremeMvcApp1.Controllers.api
{
    public class AppointmentsWidgetController : ODataController
    {
        private intomediEntities db = new intomediEntities();
        // GET: odata/AppointmentsWidget
        [EnableQuery]
        public IQueryable<av_AppointmentsWidget> GetAppointmentsWidget()
        {
            return db.av_AppointmentsWidget;
        }
    }

    public class AppointmentsController : ODataController
    {
        private intomediEntities db = new intomediEntities();
        // GET: odata/Appointments
        [EnableQuery]
        public IQueryable<av_AppointmentsWidget> GetAppointments()
        {
            return db.av_AppointmentsWidget;
        }

        // GET: odata/GetAppointment for Widget(5)
        [EnableQuery]
        public SingleResult<av_AppointmentsWidget> GetAppointmentWidget([FromODataUri] long key)
        {
            return SingleResult.Create(db.av_AppointmentsWidget.Where(av_AppointmentsWidget => av_AppointmentsWidget.ApptID == key));
        }

        // GET: odata/GetAppointment
        [EnableQuery]
        public IQueryable<av_AppointmentsWidget> GetAppointment([FromODataUri] DateTime viewdate)
        {
            return db.av_AppointmentsWidget.Where(a => a.Start >= viewdate);
        }

        // PUT: odata/av_AppointmentsWidget(5)
        public IHttpActionResult Put([FromODataUri] long key, Delta<av_AppointmentsWidget> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            av_AppointmentsWidget av_AppointmentsWidget = db.av_AppointmentsWidget.Find(key);
            if (av_AppointmentsWidget == null)
            {
                return NotFound();
            }

            patch.Put(av_AppointmentsWidget);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!av_AppointmentsWidgetExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(av_AppointmentsWidget);
        }

        // POST: odata/av_AppointmentsWidget
        public IHttpActionResult Post(av_AppointmentsWidget av_AppointmentsWidget)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.av_AppointmentsWidget.Add(av_AppointmentsWidget);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (av_AppointmentsWidgetExists(av_AppointmentsWidget.ApptID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(av_AppointmentsWidget);
        }

        // PATCH: odata/av_AppointmentsWidget(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] long key, Delta<av_AppointmentsWidget> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            av_AppointmentsWidget av_AppointmentsWidget = db.av_AppointmentsWidget.Find(key);
            if (av_AppointmentsWidget == null)
            {
                return NotFound();
            }

            patch.Patch(av_AppointmentsWidget);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!av_AppointmentsWidgetExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(av_AppointmentsWidget);
        }

        // DELETE: odata/av_AppointmentsWidget(5)
        public IHttpActionResult Delete([FromODataUri] long key)
        {
            av_AppointmentsWidget av_AppointmentsWidget = db.av_AppointmentsWidget.Find(key);
            if (av_AppointmentsWidget == null)
            {
                return NotFound();
            }

            db.av_AppointmentsWidget.Remove(av_AppointmentsWidget);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool av_AppointmentsWidgetExists(long key)
        {
            return db.av_AppointmentsWidget.Count(e => e.ApptID == key) > 0;
        }
    }
}
