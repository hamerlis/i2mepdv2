﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using DevExtremeMvcApp1;

namespace DevExtremeMvcApp1.Controllers.api
{
    public class ResourcesController : ODataController
    {
        private intomediEntities db = new intomediEntities();

        // GET: odata/ResourceAPI
        [EnableQuery]
        public IQueryable<av_ODATAResources> GetResources()
        {
            return db.av_ODATAResources.Where(av_ODATAResources => av_ODATAResources.URL != null);
        }

        // GET: odata/ResourceAPI(5)
        [EnableQuery]
        public SingleResult<av_ODATAResources> GetResource([FromODataUri] int key)
        {
            return SingleResult.Create(db.av_ODATAResources.Where(av_ODATAResources => av_ODATAResources.ResourceID == key));
        }

        // PUT: odata/ResourceAPI(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<av_ODATAResources> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            av_ODATAResources av_ODATAResources = db.av_ODATAResources.Find(key);
            if (av_ODATAResources == null)
            {
                return NotFound();
            }

            patch.Put(av_ODATAResources);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ResourceExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(av_ODATAResources);
        }

        // POST: odata/ResourceAPI
        public IHttpActionResult Post(av_ODATAResources Resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.av_ODATAResources.Add(Resource);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ResourceExists(Resource.ResourceID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(Resource);
        }

        // PATCH: odata/ResourceAPI(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<av_ODATAResources> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            av_ODATAResources av_ODATAResources = db.av_ODATAResources.Find(key);
            if (av_ODATAResources == null)
            {
                return NotFound();
            }

            patch.Patch(av_ODATAResources);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ResourceExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(av_ODATAResources);
        }

        // DELETE: odata/ResourceAPI(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            av_ODATAResources av_ODATAResources = db.av_ODATAResources.Find(key);
            if (av_ODATAResources == null)
            {
                return NotFound();
            }

            db.av_ODATAResources.Remove(av_ODATAResources);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ResourceExists(int key)
        {
            return db.av_ODATAResources.Count(e => e.ResourceID == key) > 0;
        }
    }
}
