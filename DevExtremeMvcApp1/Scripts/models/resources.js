﻿var resourcesarray

var resources = new DevExpress.data.ODataStore({
    url: "http://localhost:50188/odata/Resources",
    paginate: false,
    errorHandler: function (error) {
        DevExpress.ui.notify(error.message, "error");
    },
})

resources.on('loaded', function (result) {
    resourcesarray = result
})