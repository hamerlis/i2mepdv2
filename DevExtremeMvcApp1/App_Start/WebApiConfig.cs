using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using DevExtremeMvcApp1;

namespace DevExtremeMvcApp1 {

    public static class WebApiConfig {
        public static void Register(HttpConfiguration config) {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { action = "Get", id = RouteParameter.Optional }
            );

            
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<av_AppointmentsWidget>("Appointments");
            builder.EntitySet<av_AppointmentsWidget>("Appointments");
            builder.EntitySet<av_ODATAResources>("Resources");
            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
        }
    }

}
