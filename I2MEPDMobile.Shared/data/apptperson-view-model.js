(function() {
    Application3.ApptPersonViewModel = function(data) {
            this.ID = ko.observable();
            this.ApptID = ko.observable();
            this.PersonID = ko.observable();
            this.StatusID = ko.observable();
            this.Description = ko.observable();
            this.Color = ko.observable();
            this.Cancelled = ko.observable();
            this.VolledigeNaam = ko.observable();
            this.Geslacht = ko.observable();
            this.Geboortedatum = ko.observable();
            if(data)
                this.fromJS(data);
    };

    $.extend(Application3.ApptPersonViewModel.prototype, {
        toJS: function() {
            return {
                ID: String(this.ID() || 0),
                ApptID: String(this.ApptID() || 0),
                PersonID: String(this.PersonID() || 0),
                StatusID: this.StatusID(),
                Description: this.Description(),
                Color: this.Color(),
                Cancelled: this.Cancelled(),
                VolledigeNaam: this.VolledigeNaam(),
                Geslacht: this.Geslacht(),
                Geboortedatum: this.Geboortedatum(),
            };
        },

        fromJS: function(data) {
            if(data) {
                this.ID(data.ID);
                this.ApptID(data.ApptID);
                this.PersonID(data.PersonID);
                this.StatusID(data.StatusID);
                this.Description(data.Description);
                this.Color(data.Color);
                this.Cancelled(data.Cancelled);
                this.VolledigeNaam(data.VolledigeNaam);
                this.Geslacht(data.Geslacht);
                this.Geboortedatum(data.Geboortedatum);
            }
        },

        clear: function() {
            this.ID(undefined);
            this.ApptID(undefined);
            this.PersonID(undefined);
            this.StatusID(undefined);
            this.Description(undefined);
            this.Color(undefined);
            this.Cancelled(undefined);
            this.VolledigeNaam(undefined);
            this.Geslacht(undefined);
            this.Geboortedatum(undefined);
        }
    });
})();