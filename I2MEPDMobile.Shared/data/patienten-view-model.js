(function() {
    I2MEPDMobile.PatientenViewModel = function(data) {
            this.Geboortedatum = ko.observable();
            this.PersonID = ko.observable();
            this.Geslacht = ko.observable();
            this.TelefoonWerk = ko.observable();
            this.Telefoon = ko.observable();
            this.Email = ko.observable();
            this.TelefoonMobiel = ko.observable();
            this.BSN = ko.observable();
            this.PatientCode = ko.observable();
            this.LandISO = ko.observable();
            this.Straat = ko.observable();
            this.Huisnummer = ko.observable();
            this.WoonPlaats = ko.observable();
            this.Postcode = ko.observable();
            this.NationaliteitID = ko.observable();
            this.GeboorteLandISO = ko.observable();
            this.Roepnaam = ko.observable();
            this.LanguageID = ko.observable();
            this.Identificatie = ko.observable();
            this.Opmerkingen = ko.observable();
            this.BurgerlijkeStaatID = ko.observable();
            this.GeloofID = ko.observable();
            this.Gezinssamenstelling = ko.observable();
            this.Leefsituatie = ko.observable();
            this.VoorlopigeRegistratie = ko.observable();
            this.PolisNummer = ko.observable();
            this.IngangsDatum = ko.observable();
            this.EindDatum = ko.observable();
            this.Voornaam = ko.observable();
            this.VerzekeringsMaatschappij = ko.observable();
            this.VerzekeringsPakket = ko.observable();
            this.PersoonTypeID = ko.observable();
            this.Deleted = ko.observable();
            this.Melding = ko.observable();
            this.DeleteReason = ko.observable();
            this.Bloodgroup = ko.observable();
            this.BSNGevalideerd = ko.observable();
            this.IdentificatieGevalideerd = ko.observable();
            this.VolledigeNaam = ko.observable();
            this.Naam = ko.observable();
            this.Tussenvoegsel = ko.observable();
            this.TussenvoegselEigennaam = ko.observable();
            this.Eigennaam = ko.observable();
            this.Voorletters = ko.observable();
            this.Titel = ko.observable();
            this.ThumbURL = ko.observable();
            this.DatumOverleden = ko.observable();
            this.AanspraakZVW = ko.observable();
            this.MaatschappijCode = ko.observable();
            this.ClientID = ko.observable();
            this.Kliniek = ko.observable();
            this.ClinicOrgCode = ko.observable();
            this.GeregistreerdDoor = ko.observable();
            this.RegistratieDatum = ko.observable();
            this.Huisarts = ko.observable();
            this.HuisartsAGBCode = ko.observable();
            this.Apotheek = ko.observable();
            this.ApotheekAGBCode = ko.observable();
            this.HuisArtsPraktijkAGBCode = ko.observable();
            this.HuisArtsPraktijk = ko.observable();
            this.HasOpenTreatment = ko.observable();
            if(data)
                this.fromJS(data);
    };

    $.extend(I2MEPDMobile.PatientenViewModel.prototype, {
        toJS: function() {
            return {
                Geboortedatum: this.Geboortedatum(),
                PersonID: String(this.PersonID() || 0),
                Geslacht: this.Geslacht(),
                TelefoonWerk: this.TelefoonWerk(),
                Telefoon: this.Telefoon(),
                Email: this.Email(),
                TelefoonMobiel: this.TelefoonMobiel(),
                BSN: this.BSN(),
                PatientCode: this.PatientCode(),
                LandISO: this.LandISO(),
                Straat: this.Straat(),
                Huisnummer: this.Huisnummer(),
                WoonPlaats: this.WoonPlaats(),
                Postcode: this.Postcode(),
                NationaliteitID: this.NationaliteitID(),
                GeboorteLandISO: this.GeboorteLandISO(),
                Roepnaam: this.Roepnaam(),
                LanguageID: this.LanguageID(),
                Identificatie: this.Identificatie(),
                Opmerkingen: this.Opmerkingen(),
                BurgerlijkeStaatID: this.BurgerlijkeStaatID(),
                GeloofID: this.GeloofID(),
                Gezinssamenstelling: this.Gezinssamenstelling(),
                Leefsituatie: this.Leefsituatie(),
                VoorlopigeRegistratie: this.VoorlopigeRegistratie(),
                PolisNummer: this.PolisNummer(),
                IngangsDatum: this.IngangsDatum(),
                EindDatum: this.EindDatum(),
                Voornaam: this.Voornaam(),
                VerzekeringsMaatschappij: this.VerzekeringsMaatschappij(),
                VerzekeringsPakket: this.VerzekeringsPakket(),
                PersoonTypeID: this.PersoonTypeID(),
                Deleted: this.Deleted(),
                Melding: this.Melding(),
                DeleteReason: this.DeleteReason(),
                Bloodgroup: this.Bloodgroup(),
                BSNGevalideerd: this.BSNGevalideerd(),
                IdentificatieGevalideerd: this.IdentificatieGevalideerd(),
                VolledigeNaam: this.VolledigeNaam(),
                Naam: this.Naam(),
                Tussenvoegsel: this.Tussenvoegsel(),
                TussenvoegselEigennaam: this.TussenvoegselEigennaam(),
                Eigennaam: this.Eigennaam(),
                Voorletters: this.Voorletters(),
                Titel: this.Titel(),
                ThumbURL: this.ThumbURL(),
                DatumOverleden: this.DatumOverleden(),
                AanspraakZVW: this.AanspraakZVW(),
                MaatschappijCode: String(this.MaatschappijCode() || 0),
                ClientID: this.ClientID(),
                Kliniek: this.Kliniek(),
                ClinicOrgCode: String(this.ClinicOrgCode() || 0),
                GeregistreerdDoor: this.GeregistreerdDoor(),
                RegistratieDatum: this.RegistratieDatum(),
                Huisarts: this.Huisarts(),
                HuisartsAGBCode: this.HuisartsAGBCode(),
                Apotheek: this.Apotheek(),
                ApotheekAGBCode: this.ApotheekAGBCode(),
                HuisArtsPraktijkAGBCode: this.HuisArtsPraktijkAGBCode(),
                HuisArtsPraktijk: this.HuisArtsPraktijk(),
                HasOpenTreatment: this.HasOpenTreatment(),
            };
        },

        fromJS: function(data) {
            if(data) {
                this.Geboortedatum(data.Geboortedatum);
                this.PersonID(data.PersonID);
                this.Geslacht(data.Geslacht);
                this.TelefoonWerk(data.TelefoonWerk);
                this.Telefoon(data.Telefoon);
                this.Email(data.Email);
                this.TelefoonMobiel(data.TelefoonMobiel);
                this.BSN(data.BSN);
                this.PatientCode(data.PatientCode);
                this.LandISO(data.LandISO);
                this.Straat(data.Straat);
                this.Huisnummer(data.Huisnummer);
                this.WoonPlaats(data.WoonPlaats);
                this.Postcode(data.Postcode);
                this.NationaliteitID(data.NationaliteitID);
                this.GeboorteLandISO(data.GeboorteLandISO);
                this.Roepnaam(data.Roepnaam);
                this.LanguageID(data.LanguageID);
                this.Identificatie(data.Identificatie);
                this.Opmerkingen(data.Opmerkingen);
                this.BurgerlijkeStaatID(data.BurgerlijkeStaatID);
                this.GeloofID(data.GeloofID);
                this.Gezinssamenstelling(data.Gezinssamenstelling);
                this.Leefsituatie(data.Leefsituatie);
                this.VoorlopigeRegistratie(data.VoorlopigeRegistratie);
                this.PolisNummer(data.PolisNummer);
                this.IngangsDatum(data.IngangsDatum);
                this.EindDatum(data.EindDatum);
                this.Voornaam(data.Voornaam);
                this.VerzekeringsMaatschappij(data.VerzekeringsMaatschappij);
                this.VerzekeringsPakket(data.VerzekeringsPakket);
                this.PersoonTypeID(data.PersoonTypeID);
                this.Deleted(data.Deleted);
                this.Melding(data.Melding);
                this.DeleteReason(data.DeleteReason);
                this.Bloodgroup(data.Bloodgroup);
                this.BSNGevalideerd(data.BSNGevalideerd);
                this.IdentificatieGevalideerd(data.IdentificatieGevalideerd);
                this.VolledigeNaam(data.VolledigeNaam);
                this.Naam(data.Naam);
                this.Tussenvoegsel(data.Tussenvoegsel);
                this.TussenvoegselEigennaam(data.TussenvoegselEigennaam);
                this.Eigennaam(data.Eigennaam);
                this.Voorletters(data.Voorletters);
                this.Titel(data.Titel);
                this.ThumbURL(data.ThumbURL);
                this.DatumOverleden(data.DatumOverleden);
                this.AanspraakZVW(data.AanspraakZVW);
                this.MaatschappijCode(data.MaatschappijCode);
                this.ClientID(data.ClientID);
                this.Kliniek(data.Kliniek);
                this.ClinicOrgCode(data.ClinicOrgCode);
                this.GeregistreerdDoor(data.GeregistreerdDoor);
                this.RegistratieDatum(data.RegistratieDatum);
                this.Huisarts(data.Huisarts);
                this.HuisartsAGBCode(data.HuisartsAGBCode);
                this.Apotheek(data.Apotheek);
                this.ApotheekAGBCode(data.ApotheekAGBCode);
                this.HuisArtsPraktijkAGBCode(data.HuisArtsPraktijkAGBCode);
                this.HuisArtsPraktijk(data.HuisArtsPraktijk);
                this.HasOpenTreatment(data.HasOpenTreatment);
            }
        },

        clear: function() {
            this.Geboortedatum(undefined);
            this.PersonID(undefined);
            this.Geslacht(undefined);
            this.TelefoonWerk(undefined);
            this.Telefoon(undefined);
            this.Email(undefined);
            this.TelefoonMobiel(undefined);
            this.BSN(undefined);
            this.PatientCode(undefined);
            this.LandISO(undefined);
            this.Straat(undefined);
            this.Huisnummer(undefined);
            this.WoonPlaats(undefined);
            this.Postcode(undefined);
            this.NationaliteitID(undefined);
            this.GeboorteLandISO(undefined);
            this.Roepnaam(undefined);
            this.LanguageID(undefined);
            this.Identificatie(undefined);
            this.Opmerkingen(undefined);
            this.BurgerlijkeStaatID(undefined);
            this.GeloofID(undefined);
            this.Gezinssamenstelling(undefined);
            this.Leefsituatie(undefined);
            this.VoorlopigeRegistratie(undefined);
            this.PolisNummer(undefined);
            this.IngangsDatum(undefined);
            this.EindDatum(undefined);
            this.Voornaam(undefined);
            this.VerzekeringsMaatschappij(undefined);
            this.VerzekeringsPakket(undefined);
            this.PersoonTypeID(undefined);
            this.Deleted(undefined);
            this.Melding(undefined);
            this.DeleteReason(undefined);
            this.Bloodgroup(undefined);
            this.BSNGevalideerd(undefined);
            this.IdentificatieGevalideerd(undefined);
            this.VolledigeNaam(undefined);
            this.Naam(undefined);
            this.Tussenvoegsel(undefined);
            this.TussenvoegselEigennaam(undefined);
            this.Eigennaam(undefined);
            this.Voorletters(undefined);
            this.Titel(undefined);
            this.ThumbURL(undefined);
            this.DatumOverleden(undefined);
            this.AanspraakZVW(undefined);
            this.MaatschappijCode(undefined);
            this.ClientID(undefined);
            this.Kliniek(undefined);
            this.ClinicOrgCode(undefined);
            this.GeregistreerdDoor(undefined);
            this.RegistratieDatum(undefined);
            this.Huisarts(undefined);
            this.HuisartsAGBCode(undefined);
            this.Apotheek(undefined);
            this.ApotheekAGBCode(undefined);
            this.HuisArtsPraktijkAGBCode(undefined);
            this.HuisArtsPraktijk(undefined);
            this.HasOpenTreatment(undefined);
        }
    });
})();