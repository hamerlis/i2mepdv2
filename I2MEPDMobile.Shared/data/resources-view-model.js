(function() {
    I2MEPDMobile.ResourceViewModel = function(data) {
            this.PersonID = ko.observable();
            this.ResourceID = ko.observable();
            this.ResourceDesc = ko.observable();
            this.ResourceColor = ko.observable();
            this.Remarks = ko.observable();
            this.Email = ko.observable();
            this.deleted = ko.observable();
            this.SpecialismDesc = ko.observable();
            this.SpecialismID = ko.observable();
            this.Abbreviation = ko.observable();
            this.ResTypeDesc = ko.observable();
            this.ResourceClass = ko.observable();
            this.ResourceClassID = ko.observable();
            this.ResTypeID = ko.observable();
            this.ClientID = ko.observable();
            this.CreatedBy = ko.observable();
            this.CreatedOn = ko.observable();
            this.UpdateUserID = ko.observable();
            this.ChangedOn = ko.observable();
            this.DeletedBy = ko.observable();
            this.DeletedOn = ko.observable();
            this.Sort = ko.observable();
            this.UserID = ko.observable();
            this.BehandeldArts = ko.observable();
            this.AGBCode = ko.observable();
            this.BIGCode = ko.observable();
            this.ClinicName = ko.observable();
            this.Telefoon = ko.observable();
            this.TelefoonMobiel = ko.observable();
            this.TelefoonWerk = ko.observable();
            if(data)
                this.fromJS(data);
    };

    $.extend(I2MEPDMobile.ResourceViewModel.prototype, {
        toJS: function() {
            return {
                PersonID: String(this.PersonID() || 0),
                ResourceID: this.ResourceID(),
                ResourceDesc: this.ResourceDesc(),
                ResourceColor: this.ResourceColor(),
                Remarks: this.Remarks(),
                Email: this.Email(),
                deleted: this.deleted(),
                SpecialismDesc: this.SpecialismDesc(),
                SpecialismID: this.SpecialismID(),
                Abbreviation: this.Abbreviation(),
                ResTypeDesc: this.ResTypeDesc(),
                ResourceClass: this.ResourceClass(),
                ResourceClassID: this.ResourceClassID(),
                ResTypeID: this.ResTypeID(),
                ClientID: this.ClientID(),
                CreatedBy: this.CreatedBy(),
                CreatedOn: this.CreatedOn(),
                UpdateUserID: this.UpdateUserID(),
                ChangedOn: this.ChangedOn(),
                DeletedBy: this.DeletedBy(),
                DeletedOn: this.DeletedOn(),
                Sort: this.Sort(),
                UserID: this.UserID(),
                BehandeldArts: this.BehandeldArts(),
                AGBCode: this.AGBCode(),
                BIGCode: this.BIGCode(),
                ClinicName: this.ClinicName(),
                Telefoon : this.Telefoon() ,
                TelefoonMobiel : this.TelefoonMobiel(),
                TelefoonWerk : this.TelefoonWerk() 
            };
        },

        fromJS: function(data) {
            if(data) {
                this.PersonID(data.PersonID);
                this.ResourceID(data.ResourceID);
                this.ResourceDesc(data.ResourceDesc);
                this.ResourceColor(data.ResourceColor);
                this.Remarks(data.Remarks);
                this.Email(data.Email);
                this.deleted(data.deleted);
                this.SpecialismDesc(data.SpecialismDesc);
                this.SpecialismID(data.SpecialismID);
                this.Abbreviation(data.Abbreviation);
                this.ResTypeDesc(data.ResTypeDesc);
                this.ResourceClass(data.ResourceClass);
                this.ResourceClassID(data.ResourceClassID);
                this.ResTypeID(data.ResTypeID);
                this.ClientID(data.ClientID);
                this.CreatedBy(data.CreatedBy);
                this.CreatedOn(data.CreatedOn);
                this.UpdateUserID(data.UpdateUserID);
                this.ChangedOn(data.ChangedOn);
                this.DeletedBy(data.DeletedBy);
                this.DeletedOn(data.DeletedOn);
                this.Sort(data.Sort);
                this.UserID(data.UserID);
                this.BehandeldArts(data.BehandeldArts);
                this.AGBCode(data.AGBCode);
                this.BIGCode(data.BIGCode);
                this.ClinicName(data.ClinicName);
                this.Telefoon( data.Telefoon);
                this.TelefoonMobiel(data.TelefoonMobiel);
                this.TelefoonWerk(data.TelefoonWerk);
            }
        },

        clear: function() {
            this.PersonID(undefined);
            this.ResourceID(undefined);
            this.ResourceDesc(undefined);
            this.ResourceColor(undefined);
            this.Remarks(undefined);
            this.Email(undefined);
            this.deleted(undefined);
            this.SpecialismDesc(undefined);
            this.SpecialismID(undefined);
            this.Abbreviation(undefined);
            this.ResTypeDesc(undefined);
            this.ResourceClass(undefined);
            this.ResourceClassID(undefined);
            this.ResTypeID(undefined);
            this.ClientID(undefined);
            this.CreatedBy(undefined);
            this.CreatedOn(undefined);
            this.UpdateUserID(undefined);
            this.ChangedOn(undefined);
            this.DeletedBy(undefined);
            this.DeletedOn(undefined);
            this.Sort(undefined);
            this.UserID(undefined);
            this.BehandeldArts(undefined);
            this.AGBCode(undefined);
            this.BIGCode(undefined);
            this.ClinicName(undefined);
            this.Telefoon(undefined);
            this.TelefoonMobiel(undefined);
            this.TelefoonWerk(undefined);
        }
    });
})();