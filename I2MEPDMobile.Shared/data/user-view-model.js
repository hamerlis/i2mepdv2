(function() {
    I2MEPDMobile.UserViewModel = function (data) {
        this.UserID = ko.observable();
        this.Abbreviation = ko.observable();
        this.ResourceID = ko.observable();
        this.VolledigeNaam = ko.observable();
        this.ClientID = ko.observable();
        this.Geboortedatum = ko.observable();
        this.ID = ko.observable();
        this.PersonID = ko.observable();
        this.DatumWW = ko.observable();
        this.Actief = ko.observable();
        this.LaatsteLogin = ko.observable();
        this.Geblokkeerd = ko.observable()
        if(data)
            this.fromJS(data);
    };


    $.extend(I2MEPDMobile.UserViewModel.prototype, {
        fromJS: function (data) {
            if (data) {
                this.UserID(data.UserID);
                this.Abbreviation(data.Abbreviation);
                this.ResourceID(data.ResourceID);
                this.VolledigeNaam(data.VolledigeNaam);
                this.ClientID(data.ClientID);
                this.Geboortedatum(data.Geboortedatum);
                this.ID(data.ID);
                this.PersonID(data.PersonID);
                this.DatumWW(data.DatumWW);
                this.Actief(data.Actief);
                this.LaatsteLogin(data.LaatsteLogin);
                this.Geblokkeerd(data.Geblokkeerd);
            }
        },
    });
})();