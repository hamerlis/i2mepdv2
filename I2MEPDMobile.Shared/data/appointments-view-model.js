(function() {
    I2MEPDMobile.AppointmentsViewModel = function(data) {
            this.TimeSpan = ko.observable();
            this.ApptClassCode = ko.observable();
            this.Group = ko.observable();
            this.ApptID = ko.observable();
            this.ClientID = ko.observable();
            this.Start = ko.observable();
            this.End = ko.observable();
            this.StatusID = ko.observable();
            this.Description = ko.observable();
            this.ApptClassID = ko.observable();
            this.ChangedOn = ko.observable();
            this.CreatedOn = ko.observable();
            this.OrgCode = ko.observable();
            this.ChangedBy = ko.observable();
            this.ResourceDesc = ko.observable();
            this.VolledigeNaam = ko.observable();
            this.ID = ko.observable();
            this.Datum = ko.observable();
            this.Label = ko.observable();
            this.ResourceID = ko.observable();
            this.ApptStatus = ko.observable();
            this.ApptStatusColor = ko.observable();
            this.ApptClassColor = ko.observable();
            this.PatientPersonID = ko.observable();
            this.LocationName = ko.observable();
            this.Cancelled = ko.observable();
            this.PaStatus = ko.observable();
            this.PaStColor = ko.observable();
            this.PaCancelled = ko.observable();
            this.PaStatusID = ko.observable();
            this.CreatedBy = ko.observable();
            this.ResourceClassID = ko.observable();
            this.ResourceAbbreviation = ko.observable();
            this.ApptClassDesc = ko.observable();
            this.ResourceInfo = ko.observable();
            this.PatientMissingData = ko.observable();
            this.PatientRemarks = ko.observable()
            if(data)
                this.fromJS(data);
    };

    $.extend(I2MEPDMobile.AppointmentsViewModel.prototype, {
        toJS: function() {
            return {
                TimeSpan: this.TimeSpan(),
                ApptClassCode: this.ApptClassCode(),
                Group: this.Group(),
                ApptID: String(this.ApptID()|| 0),
                ClientID: this.ClientID(),
                Start: this.Start(),
                End: this.End(),
                StatusID: this.StatusID(),
                Description: this.Description(),
                ApptClassID: this.ApptClassID(),
                ChangedOn: this.ChangedOn(),
                CreatedOn: this.CreatedOn(),
                OrgCode: String(this.OrgCode() || 0),
                ChangedBy: this.ChangedBy(),
                ResourceDesc: this.ResourceDesc(),
                VolledigeNaam: this.VolledigeNaam(),
                ID: String(this.ID() || 0),
                Datum: this.Datum(),
                Label: this.Label(),
                ResourceID: this.ResourceID(),
                ApptStatus: this.ApptStatus(),
                ApptStatusColor: this.ApptStatusColor(),
                ApptClassColor: this.ApptClassColor(),
                PatientPersonID: String(this.PatientPersonID() || 0),
                LocationName: this.LocationName(),
                Cancelled: this.Cancelled(),
                PaStatus: this.PaStatus(),
                PaStColor: this.PaStColor(),
                PaCancelled: this.PaCancelled(),
                PaStatusID: this.PaStatusID(),
                CreatedBy: this.CreatedBy(),
                ResourceClassID: this.ResourceClassID(),
                ResourceAbbreviation: this.ResourceAbbreviation(),
                ApptClassDesc: this.ApptClassDesc(),
                ResourceInfo: this.ResourceInfo(),
                PatientMissingData: this.PatientMissingData(),
                PatientRemarks: this.PatientRemarks()
            };
        },

        fromJS: function(data) {
            if(data) {
                this.TimeSpan(data.TimeSpan);
                this.ApptClassCode(data.ApptClassCode);
                this.Group(data.Group);
                this.ApptID(data.ApptID);
                this.ClientID(data.ClientID);
                this.Start(data.Start);
                this.End(data.End);
                this.StatusID(data.StatusID);
                this.Description(data.Description);
                this.ApptClassID(data.ApptClassID);
                this.ChangedOn(data.ChangedOn);
                this.CreatedOn(data.CreatedOn);
                this.OrgCode(data.OrgCode);
                this.ChangedBy(data.ChangedBy);
                this.ResourceDesc(data.ResourceDesc);
                this.VolledigeNaam(data.VolledigeNaam);
                this.ID(data.ID);
                this.Datum(data.Datum);
                this.Label(data.Label);
                this.ResourceID(data.ResourceID);
                this.ApptStatus(data.ApptStatus);
                this.ApptStatusColor(data.ApptStatusColor);
                this.ApptClassColor(data.ApptClassColor);
                this.PatientPersonID(data.PatientPersonID);
                this.LocationName(data.LocationName);
                this.Cancelled(data.Cancelled);
                this.PaStatus(data.PaStatus);
                this.PaStColor(data.PaStColor);
                this.PaCancelled(data.PaCancelled);
                this.PaStatusID(data.PaStatusID);
                this.CreatedBy(data.CreatedBy);
                this.ResourceClassID(data.ResourceClassID);
                this.ResourceAbbreviation(data.ResourceAbbreviation);
                this.ApptClassDesc(data.ApptClassDesc);
                this.ResourceInfo(data.ResourceInfo);
                this.PatientMissingData(data.PatientMissingData);
                this.PatientRemarks(data.PatientRemarks);
            }
        },

        clear: function() {
            this.TimeSpan(undefined);
            this.ApptClassCode(undefined);
            this.Group(undefined);
            this.ApptID(undefined);
            this.ClientID(undefined);
            this.Start(undefined);
            this.End(undefined);
            this.StatusID(undefined);
            this.Description(undefined);
            this.ApptClassID(undefined);
            this.ChangedOn(undefined);
            this.CreatedOn(undefined);
            this.OrgCode(undefined);
            this.ChangedBy(undefined);
            this.ResourceDesc(undefined);
            this.VolledigeNaam(undefined);
            this.ID(undefined);
            this.Datum(undefined);
            this.Label(undefined);
            this.ResourceID(undefined);
            this.ApptStatus(undefined);
            this.ApptStatusColor(undefined);
            this.ApptClassColor(undefined);
            this.PatientPersonID(undefined);
            this.LocationName(undefined);
            this.Cancelled(undefined);
            this.PaStatus(undefined);
            this.PaStColor(undefined);
            this.PaCancelled(undefined);
            this.PaStatusID(undefined);
            this.CreatedBy(undefined);
            this.ResourceClassID(undefined);
            this.ResourceAbbreviation(undefined);
            this.ApptClassDesc(undefined);
            this.ResourceInfo(undefined);
            this.PatientMissingData(undefined);
            this.PatientRemarks(undefined);
        }
    });
})();