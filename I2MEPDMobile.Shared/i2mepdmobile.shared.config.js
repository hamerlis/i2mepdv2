// NOTE object below must be a valid JSON
window.I2MEPDMobile = $.extend(true, window.I2MEPDMobile, {
    "config": {
        "endpoints": {
            "db": {
                "local": "http://localhost:6904/odata",
                "production": "http://localhost:6904/odata"
            }
        },
        "services": {
            "db": {
                "entities": {
                    "Appointments": { 
                        "key": "ID", 
                        "keyType": "Int64"
                    },
                    "Patienten": { 
                        "key": "PersonID", 
                        "keyType": "Int64" 
                    },
                    "Resources": { 
                        "key": "ResourceID", 
                        "keyType": "Int64" 
                    },
                    "ApptPerson": {
                        "key": "ApptID", 
                        "keyType": "Int64" 
                    },
                    "ApptResource": {
                        "key": "ApptID", 
                        "keyType": "Int64" 
                    },
                    "Users": {
                        "key": "UserID",
                        "keyType": "String"
                    }
                },
                beforeSend: function (request) {
                    var tkn = sessionStorage.token
                    request.headers["Authorization"] = 'Bearer ' + tkn
                }
            }
        }
    }
});
